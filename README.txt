The intention is to provide a sample CMS backend that works with Uncle Cheese's
https://github.com/silverstripe/silverstripe-nextjs-starter

Many of the instructions below are very basic, but I want to be sure that nothing is omitted.
===========================================================
Backend - SS headless. I have this in a directory ‘backend’

cd nextjs-ss-backend-test

composer install

This will include the silverstripe vendor packages silverstripe-headless and silverstripe-nextjs

Add your silverstripe .env file, at this point containing these entries:
# For a complete list of core environment variables see
# https://docs.silverstripe.org/en/4/getting_started/environment_management/#core-environment-variables

# DB credentials
SS_DATABASE_CLASS="MySQLDatabase"
SS_DATABASE_SERVER="localhost"
SS_DATABASE_USERNAME="<user>"
SS_DATABASE_PASSWORD="<password>"
SS_DATABASE_NAME="<database>"

# WARNING: in a live environment, change this to "live" instead of dev
SS_ENVIRONMENT_TYPE=“dev"

These need to be added later:
NEXTJS_BASE_URL='<usually http://localhost:3000>'
NEXTJS_PREVIEW_KEY="<generated key>”

Setup your host with doc root the public folder of your backend

Rund composer vendor-expose in your root dir

Run <your host>/dev/build/?flush=1

Note: below the usual “Database build completed!”

You should see the building of the two graphql schemas, default and admin

===========================================================
Frontend with nextjs by Uncle Cheese

I have this in a directory ‘frontend’

Instructions here, some repeated below.
https://github.com/silverstripe/silverstripe-nextjs-starter

Install Uncle Cheese’s
https://github.com/silverstripe/silverstripe-nextjs-starter#getting-started

git clone https://github.com/silverstripe/silverstripe-nextjs-starter

cd silverstripe-nextjs-starter
git  checkout remotes/origin/content-blocks-theme
git switch content-blocks-theme (I want the version using content blocks)

git status will show:
* content-blocks-theme
  main
  remotes/origin/HEAD -> origin/main
  remotes/origin/base
  remotes/origin/base-theme
  remotes/origin/content-blocks
  remotes/origin/content-blocks-theme
  remotes/origin/main

yarn install

yarn setup

This should write three entries into the .env file after completion


Note Uncle Cheese has instructions under

Expose your data
I have used the following config for _config/nextjs.yml
SilverStripe\Headless\GraphQL\ModelLoader:
  included_dataobjects:
    page: 'Page'
    cms: 'SilverStripe\CMS\Model\SiteTree'
    siteconfig: 'SilverStripe\SiteConfig\*'
    assets: 'SilverStripe\Assets\*'
    elemental: ‘DNADesign\Elemental\Models\*'

(Differs from his entry)


Now say your prayers and run
yarn run dev

Your skeletal front end should display at localhost:3000
You can add some content blocks to the SS pages
(It seems the Titles of content blocks dont show up in the front end at the moment)

If the page editing with preview doesn’t work completely

Check that the entry in the frontend and backend .env files for
 SILVERSTRIPE_PREVIEW_KEY are the same

============MISCELLANEOUS =======
Useful too if there are odd issues with graphql
<your cms url>/dev/graphql/build?schema=default&clear=1
<your cms url>/dev/graphql/build?schema=admin&clear=1

Your Graphql Playground should be available here:
<your cms url>>/dev/graphql/ide (differs from Graphql 3 )

query {
  readPages {
    nodes {
      title
    }
  }
}

Should give you something like
{
  "data": {
    "readPages": {
      "nodes": [
        {
          "title": "Home"
        },
        {
          "title": "About Us"
        },
        {
          "title": "Contact Us"
        },
        {
          "title": "Page not found"
        },
        {
          "title": "Server error"
        }
      ]
    }
  }
}

